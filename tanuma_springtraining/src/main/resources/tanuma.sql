-- MySQL dump 10.13  Distrib 5.6.47, for osx10.15 (x86_64)
--
-- Host: localhost    Database: simple_twitter
-- ------------------------------------------------------
-- Server version	5.6.47

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `branch_name` varchar(20) NOT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'本社'),(2,'支店A'),(3,'支店B'),(4,'支店C');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `position_id` int(11) NOT NULL DEFAULT '0',
  `position_name` varchar(20) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` VALUES (1,'総務人事担当者'),(2,'情報管理担当者'),(3,'支店長'),(4,'社員1'),(5,'社員2'),(6,'社員3');
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login_id` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test11','$2a$10$eeXAkAhv8FBL/yOyzFyxKu62KsqO/iD3A1QIZkLPyAu3325QbMXOy','t',2,3,1,'2020-12-11 05:02:20','2020-12-14 02:25:23'),(2,'987654321','$2a$10$vPn6V4veQtfeYJY8j1Zd1.aP2DWp5czZj.JDyvFhGvErkN3IGNGwS','testあ',1,2,1,'2020-12-11 05:02:49','2020-12-17 06:36:32'),(3,'11111111','$2a$10$gtEcd4Xc.fCSbQSPcw06f.s5c47COIBg1xsXkUEP4dw27w0AZfWU2','sqldata',3,3,0,'2020-12-11 05:03:30','2020-12-11 05:49:44'),(4,'999999999','$2a$10$7WGMyGncT3SszB7xCPfMtu2V6aN7vNoKnc6YFvTTwd48kZE05G7gS','twitter',4,4,0,'2020-12-11 05:04:02','2020-12-11 05:50:00'),(5,'101010101','$2a$10$/R5MqCWzHFaCKp3bgX/FcuV.yg.UocQwcDX/H1GW.SnP8duc8D3FK','java',2,4,0,'2020-12-11 05:04:44','2020-12-11 05:49:47'),(6,'10101010','$2a$10$HWayz6MT8tcnWyF.aej7new/YQHc19vs8eL/QxjZmCzOAstV.x0pK','jsp',4,4,0,'2020-12-11 05:05:34','2020-12-11 05:49:57'),(7,'javajava','$2a$10$r3WtBnWry0hFcirsTg5MAOGCAoAvwXF8eXS5SAvU.IwR5JukIaEp.','javajava',3,3,0,'2020-12-11 05:06:06','2020-12-11 05:49:50'),(8,'AAAA11','$2a$10$xi/IkDIOqF8hP.gwuWlilevcJ3nk/tumCQ54WWqq6ewYeek7QG65u','test',1,1,1,'2020-12-11 05:09:18','2020-12-17 07:02:38'),(9,'aaaaa1011','$2a$10$I0ExFxTtj/uye0S1u.KIiO5aGazKO6y7c0K8CVPGoECMoN7MCBKFy','aaaaa',4,5,0,'2020-12-11 05:35:03','2020-12-11 05:49:54'),(10,'aaaaaA','$2a$10$f11SInIhWgQpvmYP20wcf..YQ7f0gwha/pK3gsEhQRAZd.m6DFtJ.','test111',1,1,1,'2020-12-11 05:39:52','2020-12-15 09:59:14'),(11,'aaa111','$2a$10$O.Y8DNXGbBH7dK4wGfF8Yu96emNsmAEddtTjFg9RyZkmcjLyML.DW','aaa111',1,1,0,'2020-12-11 06:01:25','2020-12-11 06:01:25'),(12,'aaaaaaaaaaaaaaaaaaa1','$2a$10$0BYFiPu/S4oQyjv2QbamjuWxOQjndkLAudoG9c8q33mpvWUieow3.','test000000',1,2,0,'2020-12-11 06:07:48','2020-12-11 06:07:48'),(13,'aaaaa1','$2a$10$ayfROIqdOK1YYPZAbFnbi./g3TDv4otawSP7PqVlxK8zjwYmGUgrK','aaaaa1aaaa',1,1,0,'2020-12-11 06:10:22','2020-12-11 06:10:22'),(14,'test00','$2a$10$fi8UQxSpSBBd5/5uNG/spuFWDXZmEvtJr/G2NPh9C8LO.zlwhx9pu','test111111',4,5,1,'2020-12-11 06:12:44','2020-12-14 02:30:55'),(15,'test22','$2a$10$7W/fI1IRYEEvTn5EOUSCTuF3Xz8f2ja9c0AF83.WK.mKL4tXoc4Yu','test222222',4,6,1,'2020-12-11 06:15:31','2020-12-14 02:33:10'),(16,'test33','$2a$10$B8nGpk7tq13abbP92JHXPeHvmFhkj1nxqqq/Ykar1XTfSae./tKgK','test333333',4,5,0,'2020-12-11 06:17:25','2020-12-11 06:17:25'),(17,'test44','$2a$10$TlvV2pZEGB7MMsE.rcDKO.2z5Qtbj7mglbkEGuZEFW17A4uE6oxoS','a',4,6,0,'2020-12-11 06:22:45','2020-12-11 06:22:45'),(18,'javajava1','$2a$10$WQg5kN77SaBQZTOS7igPQO52J9IJYhl0LSrSx2kKmN0q.FS9IdTwO','test5test5',1,1,0,'2020-12-11 06:52:03','2020-12-16 01:55:29'),(19,'test66','$2a$10$SQUd720QagMA8iVstF2qRewk5poR6WIq0Mf1irS8SNBhfxNLbopG2','test6test6',1,1,0,'2020-12-11 07:07:32','2020-12-11 07:07:32'),(32,'javajavaaa','$2a$10$aDVO6058e5V3q9jt5uU7b.SsxyODjXQnt9rpTQzYriUPDIrD88C1u','aaaaaaa',2,1,0,'2020-12-16 08:10:05','2020-12-16 08:10:05'),(33,NULL,'$2a$10$zilb0NrIKlFn9aCqTgaOeeAevcknKRmj36EZe9aDKR2iAms79A/82','test6test6',1,1,0,'2020-12-16 08:10:21','2020-12-16 08:10:21'),(34,NULL,'$2a$10$Ge4.cNBqvn.trM5DWRD/5OmhZf8DLOCJO5yHoUqxos6s3tAZWBYiC','test6test6',2,1,0,'2020-12-16 08:10:47','2020-12-16 08:10:47'),(35,NULL,'$2a$10$mwwohqkLYaNK5vU7W/w.T.c6eyJRHyMyIzzlRcPeKTH45Q89PUTG.','test6test6',1,1,0,'2020-12-16 08:13:16','2020-12-16 08:13:16'),(36,'aaaaa1231','$2a$10$.T5PRtziWvosVjM6D2c2TOT9SBlWBODOtAcQeV0VMf1TkbBLVVx3S','test123',1,3,0,'2020-12-16 08:56:09','2020-12-16 08:56:09'),(37,'aaaaa1234','$2a$10$dPMNDG9GjlgbZ13bd7Ke5eT/hdv89GIxsDOvAtWi4H4nlBpB7DPFC','aaaaaA',1,1,0,'2020-12-17 02:09:27','2020-12-17 02:09:27'),(38,'10aaaa10','$2a$10$ck1K/HojNTytuuzvSFZdCO80TtuNUDB2.qGItoFtNVphXQ032p6km','aaa１',1,2,0,'2020-12-17 02:10:04','2020-12-17 02:10:04');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-17 16:28:30
