/* 【編集画面】ログインIDエラーメッセージ */
$(function() {
	$("input[name='login_id']").blur(function() {
		if($(this).val() == ""){
			alert('ログインIDを入力してください');
		} else if($(this).val().length < 6 || $(this).val().length > 20){
			alert('ログインIDは6~20文字の半角英数字で入力してください');
		} else if(!$(this).val.match(/^[0-9a-zA-Z]/)){
			alert('ログインIDは6~20文字の半角英数字で入力してください');
		}
	});
});

/* 【編集画面】名称エラーメッセージ */
$(function() {
	$("input[name='name']").blur(function() {
		if($(this).val() == ""){
			alert('名称を入力してください');
		} else if($(this).val().length > 10){
			alert('名称は10文字以下で入力してください');
		}
	});
});

/* 【編集画面】パスワードエラーメッセージ */
$(function() {
	$("input[name='rawPassword']").blur(function() {
		if(!$(this).val().match(/^[a-z0-9!-/:-@-`{-~]{6,20}/)){
			alert('変更するパスワードは6~20文字の記号を含む半角英数字で入力してください');
		}
	});
});

/* 【編集画面】パスワード(確認用)エラーメッセージ */
$(function() {
	$("input[name='confirmationPassword']").change(function(e) {
		if(!$(this).val().match(/^[a-z0-9!-/:-@-`{-~]{6,20}/)){
			alert('確認用パスワードは6~20文字の記号を含む半角英数字で入力してください');
		}
		const type = e.target.type;
		const validity = e.target.validity;
		const form = document.getElementById('form');
		const error = $('#error');
		const password = form.elements['rawPassword'];
		const passwordConfirm = form.elements['confirmationPassword'];
		var msg = "";
		// パスワードと新しいパスワードが一致しない場合、エラーをセットする
		if (password.value !== passwordConfirm.value) {
			msg = "パスワードが一致しません";
		}
		// エラーメッセージ
		error.html( msg );
	});
});

/* 【編集画面】支店エラーメッセージ */
$(function() {
	$(".branch").blur(function() {
		if($(this).val() == ""){
			alert('選択してください');
		}
	});
});

/* 【編集画面】部署エラーメッセージ */
$(function() {
	$(".position").blur(function() {
		if($(this).val() == ""){
			alert('選択してください');
		}
	});
});
