/* 【ホーム画面】復活ボタン押下時 */
$(function() {
	$("button[id=btnRestart]").click( function() {
		if(!confirm('復活しますか？')) {
			return false;
		} else {
			var id = $(this).attr('data-user_id');
			var url = '/users/restart/' + id;
			window.location.href= url;
			return true;
		}
	});
});

/* 【ホーム画面】停止ボタン押下時 */
$(function() {
	$("button[id=btnStop]").click( function() {
		if(!confirm('停止しますか？')) {
			return false;
		} else {
			var id = $(this).attr('data-user_id');
			var url = '/users/stop/' + id;
			window.location.href= url;
			return true;
		}
	});
});

