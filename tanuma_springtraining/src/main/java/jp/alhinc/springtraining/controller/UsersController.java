package jp.alhinc.springtraining.controller;

import java.util.List;

import javax.swing.text.Position;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.service.ChangeActiveService;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.EditUserService;
import jp.alhinc.springtraining.service.GetAllBranchesService;
import jp.alhinc.springtraining.service.GetAllPositionsService;
import jp.alhinc.springtraining.service.GetAllUsersService;

@Controller
@RequestMapping("/users")
public class UsersController {

	//@Autowiredはnewを省略できる
	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
    private GetAllBranchesService getAllBranchesService;

	@Autowired
    private GetAllPositionsService getAllPositionsService;

	@Autowired
    private EditUserService editUserService;

	@Autowired
    private ChangeActiveService changeActiveService;

	//ユーザー一覧画面表示
	@GetMapping
	public String index(Model model) {
		//全ユーザー情報取得
		List<User> users = getAllUsersService.getAllUsers();
		model.addAttribute("users", users);
		return "users/index";
	}

	//新規登録画面表示(GET=取得)
	@GetMapping("/create")
	public String create(CreateUserForm form, Model model) {
		List<Branch> branches = getAllBranchesService.getAllBranches();
		List<Position> positions = getAllPositionsService.getAllPositions();
		model.addAttribute("branches", branches);
		model.addAttribute("positions", positions);
		model.addAttribute("form", form);
		return "users/create";
	}

	//ユーザー新規登録(POST=登録、更新)
	@PostMapping("/create")
	public String create(@Validated @ModelAttribute("form") CreateUserForm form, BindingResult result, Model model) {
		if (!createUserService.isLoginidValid(form)) {
			FieldError fieldError = new FieldError(result.getObjectName(),"loginIdErrorMessage", "すでに登録されたログインIDです");
			result.addError(fieldError);
		}
		//エラー時の対応
		if (result.hasErrors()) {
			List<Branch> branches = getAllBranchesService.getAllBranches();
			List<Position> positions = getAllPositionsService.getAllPositions();
			model.addAttribute("message", "残念");
			model.addAttribute("branches", branches);
			model.addAttribute("positions", positions);
			model.addAttribute("form", form);
			return "/users/create";
		}
		//登録完了
		createUserService.create(form);
		return "redirect:/users";
	}

	//ユーザー編集画面表示(GET=取得)
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable Long id, EditUserForm form, Model model) {
		User user = editUserService.findEditUser(id);
		List<Branch> branches = getAllBranchesService.getAllBranches();
		List<Position> positions = getAllPositionsService.getAllPositions();
		form.setId(user.getId());
	    form.setLogin_id(user.getLogin_id());
	    form.setName(user.getName());
	    form.setBranch_id(user.getBranch_id());
	    form.setPosition_id(user.getPosition_id());
	    model.addAttribute("branches", branches);
		model.addAttribute("positions", positions);
	    model.addAttribute("form", form);
	    return "users/edit";
	}


	//ユーザー編集情報登録(POST=登録、更新)
	@PostMapping("/update/{id}")
	public String update(@PathVariable Long id, @Validated @ModelAttribute("form") EditUserForm form, BindingResult result, Model model) {
		if (!editUserService.isLoginidValid(form)) {
			FieldError fieldError = new FieldError(result.getObjectName(),"loginIdErrorMessage", "すでに登録されたログインIDです");
			result.addError(fieldError);
		}
		if (result.hasErrors()) {
			List<Branch> branches = getAllBranchesService.getAllBranches();
			List<Position> positions = getAllPositionsService.getAllPositions();
			model.addAttribute("branches", branches);
			model.addAttribute("positions", positions);
	    	model.addAttribute("form", form);
	    	return "/users/edit";
	    }
	    editUserService.edit(form, id);
	    return "redirect:/users";
	}

	@GetMapping("/stop/{id}")
	public String stop(@PathVariable Long id, Model model) {
		User user = changeActiveService.findEditUser(id);
	    if(user == null) {
	    	return "redirect:/users";
	    }
	    user.setStatus(Byte.valueOf("0"));
	    changeActiveService.change(user);
	    return "redirect:/users";
	}

	@GetMapping("/restart/{id}")
	public String restart(@PathVariable Long id, Model model) {
		User user = changeActiveService.findEditUser(id);
	    if(user == null) {
	    	return "redirect:/users";
	    }
	    user.setStatus(Byte.valueOf("1"));
	    changeActiveService.change(user);
	    return "redirect:/users";
	}
}
