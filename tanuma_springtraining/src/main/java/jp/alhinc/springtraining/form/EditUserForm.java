package jp.alhinc.springtraining.form;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditUserForm implements Serializable {

	private Long id;

	@NotBlank(message = "ログインIDを入力してください")
	private String login_id;

	@AssertTrue(message = "ログインIDは6~20文字の半角英数字で入力してください")
	public boolean isLoginIdValid() {
		String regexLoginId = "[a-zA-Z0-9]{6,20}";
		Pattern loginIdPattern = Pattern.compile(regexLoginId);
		Matcher loginIdMatcher = loginIdPattern.matcher(login_id);
		if (loginIdMatcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	@NotBlank(message = "名称を入力してください")
	@Size(max = 10, message = "名称は10文字以内で入力してください")
	private String name;

	private String rawPassword;

	@AssertTrue(message = "パスワードは6~20文字の記号を含む半角英数字で入力してください")
	public boolean isRawPasswordValid() {
		String regexRawPassword = "^[a-zA-Z0-9!-\\/:-@\\[-`\\{-~\\]*\\$]{6,20}";
		Pattern rawPasswordPattern = Pattern.compile(regexRawPassword);
		Matcher rawPasswordMatcher = rawPasswordPattern.matcher(rawPassword);
		if (rawPassword == null || rawPassword.equals("")) {
			return true;
		} else if (rawPasswordMatcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	private String confirmationPassword;

	@AssertTrue(message = "確認用パスワードは6~20文字の記号を含む半角英数字で入力してください")
	public boolean isConfirmationPasswordValid() {
		String regexConfirmationPassword = "^[a-zA-Z0-9!-\\/:-@\\[-`\\{-~\\]*\\$]{6,20}";
		Pattern confirmationPasswordPattern = Pattern.compile(regexConfirmationPassword);
		Matcher confirmationPasswordMatcher = confirmationPasswordPattern.matcher(rawPassword);
		if (rawPassword == null || rawPassword.equals("")) {
			return true;
		} else if (confirmationPasswordMatcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	@AssertTrue(message = "パスワードが一致しません")
	public boolean isPasswordValid() {
		if(rawPassword == null || rawPassword.isEmpty()) {
			return true;
		}
		return rawPassword.equals(confirmationPassword);
	}

	@NotNull(message = "選択してください")
	private Integer branch_id;

	@NotNull(message = "選択してください")
	private Integer position_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRawPassword() {
		return rawPassword;
	}

	public void setRawPassword(String rawPassword) {
		this.rawPassword = rawPassword;
	}

	public String getConfirmationPassword() {
		return confirmationPassword;
	}

	public void setConfirmationPassword(String confirmationPassword) {
		this.confirmationPassword = confirmationPassword;
	}

	public Integer getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(Integer branch_id) {
		this.branch_id = branch_id;
	}

	public Integer getPosition_id() {
		return position_id;
	}

	public void setPosition_id(Integer position_id) {
		this.position_id = position_id;
	}

	private String loginIdErrorMessage;

	public String getLoginIdErrorMessage() {
		return loginIdErrorMessage;
	}

	public void setLoginIdErrorMessage(String loginIdErrorMessage) {
		this.loginIdErrorMessage = loginIdErrorMessage;
	}
}
