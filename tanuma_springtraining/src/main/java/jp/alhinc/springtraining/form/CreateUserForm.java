package jp.alhinc.springtraining.form;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CreateUserForm implements Serializable {

	private Long id;

	@NotNull(message = "ログインIDを入力してください")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}", message = "ログインIDは6~20文字の半角英数字で入力してください")
	private String login_id;

	@NotNull(message = "名称を入力してください")
	@Size(max = 10, message = "名称は10文字で入力してください")
	private String name;

	@NotNull(message = "パスワードを入力してください")
	@Pattern(regexp = "^[a-z0-9!-/:-@-`{-~]{6,20}", message = "パスワードは6~20文字の記号を含む半角英数字で入力してください")
	private String rawPassword;

	@NotNull(message = "確認用パスワードを入力してください")
	@Pattern(regexp = "^[a-z0-9!-/:-@-`{-~]{6,20}", message = "確認用パスワードは6~20文字の記号を含む半角英数字で入力してください")
	private String confirmationPassword;

	@AssertTrue(message = "パスワードが一致しません")
	public boolean isPasswordValid() {
		if(rawPassword == null || rawPassword.isEmpty()) {
			return false;
		}
		return rawPassword.equals(confirmationPassword);
	}

	@NotNull(message = "選択してください")
	private Integer branch_id;

	@NotNull(message = "選択してください")
	private Integer position_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRawPassword() {
		return rawPassword;
	}

	public void setRawPassword(String rawPassword) {
		this.rawPassword = rawPassword;
	}

	public String getConfirmationPassword() {
		return confirmationPassword;
	}

	public void setConfirmationPassword(String confirmationPassword) {
		this.confirmationPassword = confirmationPassword;
	}

	//form内のid取得
	public Integer getBranch_id() {
		return branch_id;
	}

	//データ格納
	public void setBranch_id(Integer branch_id) {
		this.branch_id = branch_id;
	}

	public Integer getPosition_id() {
		return position_id;
	}

	public void setPosition_id(Integer position_id) {
		this.position_id = position_id;
	}

	private String loginIdErrorMessage;

	public String getLoginIdErrorMessage() {
		return loginIdErrorMessage;
	}

	public void setLoginIdErrorMessage(String loginIdErrorMessage) {
		this.loginIdErrorMessage = loginIdErrorMessage;
	}
}
