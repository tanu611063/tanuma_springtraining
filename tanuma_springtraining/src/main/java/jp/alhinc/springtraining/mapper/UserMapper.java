package jp.alhinc.springtraining.mapper;

import java.util.List;

import javax.swing.text.Position;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.User;

@Mapper
public interface UserMapper {

	List<User> findAll();

	List<Branch> findBranches();

	List<Position> findPositions();

	int create(User entity);

	int changeStatus(User entity);

	User findEditUser(Long id);

	int edit(User entity);

	int findExistsUser(String login_id);
}
