package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class CreateUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public int create(CreateUserForm form) {
		//新規登録する情報をセット
		User entity = new User();
		entity.setName(form.getName());
		entity.setLogin_id(form.getLogin_id());
		entity.setBranch_id(form.getBranch_id());
		entity.setPosition_id(form.getPosition_id());
		entity.setStatus(Byte.valueOf("0"));
		//パスワード暗号化処理
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRawPassword()));
		//DB登録
		return mapper.create(entity);
	}

	//ログインID重複確認
	public boolean isLoginidValid(CreateUserForm form) {
		if(mapper.findExistsUser(form.getLogin_id()) >= 1)  return false;
		return true;
	}
}
