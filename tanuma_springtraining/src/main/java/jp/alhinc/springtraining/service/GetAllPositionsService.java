package jp.alhinc.springtraining.service;

import java.util.List;

import javax.swing.text.Position;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class GetAllPositionsService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public List<Position> getAllPositions() {
		return mapper.findPositions();
	}
}
