package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class EditUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public User findEditUser(Long id) {
		return mapper.findEditUser(id);
	}

	@Transactional
	public int edit(EditUserForm form, Long id) {
		//編集する情報
		User entity = new User();
		entity.setId(id);
		entity.setName(form.getName());
		entity.setLogin_id(form.getLogin_id());
		entity.setBranch_id(form.getBranch_id());
		entity.setPosition_id(form.getPosition_id());
		//パスワード暗号化処理
		if(form.getRawPassword() != null) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			entity.setPassword(encoder.encode(form.getRawPassword()));
		}
		//DB登録
		return mapper.edit(entity);
	}

	public boolean isLoginidValid(EditUserForm form) {
		//保存されたログインIDの重複確認
		if(mapper.findExistsUser(form.getLogin_id()) >= 1) {
			return false;
		}
		return true;
	}
}

