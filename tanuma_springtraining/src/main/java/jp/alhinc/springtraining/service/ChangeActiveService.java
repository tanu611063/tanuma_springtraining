package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class ChangeActiveService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public User findEditUser(Long id) {
		return mapper.findEditUser(id);
	}

	@Transactional
	public int change(User entity) {
		return mapper.edit(entity);
	}
}
